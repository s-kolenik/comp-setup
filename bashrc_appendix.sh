#!/bin/bash

bind '"\e[B": history-search-forward'
bind '"\e[A": history-search-backward'
bind '"\ej": history-search-forward'
bind '"\ek": history-search-backward'
bind '"\e;": accept-line'
bind '"\eh": "\e[D"'
bind '"\el": "\e[C"'
bind '"\e[Z": menu-complete'
bind '"\ep": "cd ..\n"'
bind '"\eu": "../"'
bind '"\em": "make\n"'

# puvodni hodnoty: xset r rate 660 25
xset r rate 200 25

alias xo=xdg-open
alias mc='EDITOR=gvim mc'

# number of lines returned by the "history" command
HISTSIZE=1000
# number of lines in $HISTFILE, file truncated when set
HISTFILESIZE=10000

# supress GTK warning
_supress() {
  eval "$1() { \$(which $1) \"\$@\" 2>&1 | tr -d '\r' | grep -v \"$2\"; }"
}
_supress xdg-open       "Failed to\|^$\|gtk_window_resize\|Gtk-WARNING"

# FZF shell completion - override completion function
_fzf_compgen_path() {
  if [ "$FZF_HIDDEN" = "true" ]; then
    echo "$1"
    command find -L "$1" \
      -name .git -prune -o -name .hg -prune -o -name .svn -prune -o \( -type d -o -type f -o -type l \) \
      -a -not -path "$1" -print 2> /dev/null | sed 's@^\./@@'
  else
    echo "$1"
    command find -L "$1" \
      -name .git -prune -o -name .hg -prune -o -name .svn -prune -o -name ".?*" -prune -o \( -type d -o -type f -o -type l \) \
      -a -not -path "$1" -print 2> /dev/null | sed 's@^\./@@'
  fi
}
export _fzf_compgen_path

# FZF shell completion - define key bindings
bind '"\ef": "\e1**\t"'
bind -x '"\e1": "export FZF_HIDDEN=false"'
bind '"\eF": "\e2**\t"'
bind -x '"\e2": "export FZF_HIDDEN=true"'

# FZF shell completion - source commands from FZF
[ -f ~/.fzf.bash ] && source ~/.fzf.bash
