#!/bin/bash

function create_link () { # (target, link name)
  if [ -e "$2" ]; then
    if [ ! -L "$2" ]; then
      read -p "  Replace $2 with a symbolic link? [Y/n] "
      if [ "$REPLY" != "n" ]; then
        rm -r $2
        ln -s "$1" "$2"
        echo "  Done"
      else
        echo "  Skipping"
      fi
    else
      echo "  Already up to date"
    fi
  else
    ln -s "$1" "$2"
    echo "  Done"
  fi
}

function install_check () { # (executable path, apt package name)
  if [ ! -x "$1" ]; then
    read -p "  $1 not found, install $2? [Y/n] "
    if [ "$REPLY" != "n" ]; then
      sudo apt install $2
      echo "  Done"
    else
      echo "  Skipping installation of $2"
    fi
  else
    echo "  Already installed: $2"
  fi
}

# get the location of this file
cd `dirname "${BASH_SOURCE[0]}"`
SETUP_DIR=$PWD

echo "======================================="
echo "> Program check"
install_check /usr/bin/tree tree
install_check /usr/bin/git git
install_check /usr/bin/i3 i3
install_check /usr/bin/xbacklight xbacklight
install_check /usr/bin/gvim vim-gnome

echo "======================================="
echo "> ~/bin setup"
create_link "$SETUP_DIR/bin" "$HOME/bin"

echo "======================================="
echo "> i3 config setup"
mkdir -p ~/.config/i3
create_link "$SETUP_DIR/i3/i3_config" "$HOME/.config/i3/config"

echo "======================================="
echo "> i3status config setup"
mkdir -p ~/.config/i3status
create_link "$SETUP_DIR/i3/i3status_config" "$HOME/.config/i3status/config"

echo "======================================="
echo "> font awesome setup"
if [ ! -f ~/.fonts/fontawesome-webfont.ttf ]; then
  mkdir -p ~/.fonts
  cp $SETUP_DIR/i3/fontawesome-webfont.ttf ~/.fonts/
  echo "  Done"
else
  echo "  Already up to date"
fi

echo "======================================="
echo "> Gvim setup"
if [ -d ~/.vim ]; then
  if [ ! -d ~/.vim/.git ]; then
    read -p "  Directory ~/.vim already exists, replace? [Y/n] "
    if [ "$REPLY" != "n" ]; then
      git clone --recurse-submodules https://gitlab.fit.cvut.cz/kolensta/vim.git ~/.vim
      echo "  Done"
    else
      echo "  Skipping"
    fi
  else
    echo "  Already up to date"
  fi
else
  mkdir -p ~/.vim
  git clone --recurse-submodules https://gitlab.fit.cvut.cz/kolensta/vim.git ~/.vim
  echo "  Done"
fi

echo "======================================="
echo "> bashrc setup"
if grep -q "HISTFILESIZE=" ~/.bashrc; then
  echo "  Removing HISTFILESIZE setting"
  grep -v "HISTFILESIZE=" ~/.bashrc >~/.bashrc.tmp
  mv ~/.bashrc.tmp ~/.bashrc
fi
if ! grep -q "$SETUP_DIR/bashrc_appendix.sh" ~/.bashrc; then
  echo "" >>~/.bashrc
  echo "# add a git-controlled appedix" >>~/.bashrc
  echo "source $SETUP_DIR/bashrc_appendix.sh" >>~/.bashrc
  echo "  Done"
else 
  echo "  Already up to date"
fi

echo "======================================="
echo "> FZF setup"
if [ -d ~/.fzf ]; then
  echo "  Already up to date"
else
  read -p "  Install FZF [Y/n] "
  if [ "$REPLY" != "n" ]; then
    git clone --depth 1 https://github.com/junegunn/fzf.git ~/.fzf
    printf "y\ny\nn\n" | ~/.fzf/install
  fi
fi
